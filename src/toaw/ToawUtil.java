package toaw;

import arestools.io.AresToolsIO;
import java.io.File;
import java.util.List;
import toaw.data.ToawUnitIcon;
import toaw.data.jaxb.GAME;
import toaw.data.jaxb.GAME.MAP;
import toaw.data.jaxb.GAME.MAP.CELL;
import toaw.data.jaxb.GAME.OOB;
import toaw.data.jaxb.GAME.OOB.FORCE;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION.UNIT;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION.UNIT.EQUIPMENT;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ToawUtil {

    public static MAP generateTestMap(int rows, int columns) {

        String b = "/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0";
        // b represents a tile using 47 layers (/0) = layer, 
        // where each layer can get a value in [0, 64]
        int nLayers = b.length() / 2; // num. de 
        int minLength = nLayers * 2;
        MAP map = new MAP();
        map.setMaxx((short) (columns - 1));
        map.setMaxy((short) (rows - 1));
        List<CELL> cells = map.getCELL();
        for (int i = 0; i < map.getMaxx() + 1; i++) {
            for (int j = 0; j < map.getMaxy() + 1; j++) {
                StringBuilder sb = new StringBuilder(minLength);
                for (int x = 0; x < nLayers; x++) {
                    if (i == x) {
                        sb.append('/');
                        sb.append(j);
                    } else {
                        sb.append('/');
                        sb.append('0');
                    }
                }
                CELL cell = new CELL();
                cell.setLoc(i + "," + j);
                cell.setB(sb.toString());
                cells.add(cell);
            }
        }
        return map;
    }

    public static MAP generateTestMap() {
        return generateTestMap(90, 48);
    }

    public static GAME generateTestUnitTypeScenario(File f) {

        ToawUnitIcon[] allToawUnitIcons = ToawUnitIcon.values();
        GAME game = (GAME) AresToolsIO.openToawFile(f);
        OOB oob = new OOB();
        game.setOOB(oob);
        for (short j = 0; j < 2; j++) {
            FORCE force = new FORCE();
            force.setID((byte) (j + 1));
            force.setNAME("FORCE " + j);
            force.setFlag(j);
            force.setFORMATIONUNASSIGNED(null);
            for (short k = 0; k < 4; k++) {
                FORMATION formation = new FORMATION();
                formation.setID((short) (k + 1));
                formation.setNAME("Formation " + (k + 1));
                force.getFORMATION().add(formation);
                oob.getFORCE().add(force);
                List<UNIT> units = formation.getUNIT();
                for (short i = 0; i < 32; i++) {
                    UNIT u = new FORMATION.UNIT();
                    units.add(u);
                    int id = k * 32 + i;
                    u.setID((short) (id + 1));
                    u.setNAME(allToawUnitIcons[id].getToawName());
                    u.setICON(allToawUnitIcons[id].getToawName());
                    u.setICONID((short) id);
                    u.setSIZE("Battalion");
                    u.setCOLOR((short) (j * 10 + k));
                    u.setSTATUS((short) 20);
                    u.setX((short) (j * 10 + id / 16));
                    u.setY((short) (id % 16));
                    u.setEMPHASIS("Minimize Losses");
                    u.setEXPERIENCE("veteran");
                    //u.setCHARACTERISTICS("4001");
                    u.setPROFICIENCY((short) 100);
                    u.setREADINESS((short) 100);
                    u.setSUPPLY((short) 100);
                    EQUIPMENT e = new EQUIPMENT();
                    u.getEQUIPMENT().add(e);
                    e.setID((byte) 1);
                    e.setNAME("Rifle Squad");
                    e.setNUMBER((short) 30);
                    e.setMAX((short) 30);
                }
            }
        }
        return game;
    }

    public static GAME generateTestStatusScenario(File f) {

        ToawUnitIcon[] allToawUnitIcons = ToawUnitIcon.values();
        GAME game = (GAME) AresToolsIO.openToawFile(f);
        OOB oob = new OOB();
        game.setOOB(oob);

        for (short j = 0; j < 2; j++) {
            FORCE force = new FORCE();
            force.setID((byte) (j + 1));
            force.setNAME("FORCE " + j);
            force.setFlag(j);
            force.setFORMATIONUNASSIGNED(null);
            for (short k = 0; k < 4; k++) {
                FORMATION formation = new FORMATION();
                formation.setID((short) (k + 1));
                formation.setNAME("Formation " + (k + 1));
                force.getFORMATION().add(formation);
                oob.getFORCE().add(force);
                List<UNIT> units = formation.getUNIT();
                for (short i = 0; i < 32; i++) {
                    UNIT u = new FORMATION.UNIT();
                    units.add(u);
                    int id = k * 32 + i;
                    u.setID((short) (id + 1));
                    u.setNAME(allToawUnitIcons[id].getToawName());
                    u.setICON(allToawUnitIcons[id].getToawName());
                    u.setICONID((short) id);
                    u.setSIZE("Battalion");
                    u.setCOLOR((short) (j * 10 + k));
                    u.setSTATUS(i);
                    u.setX((short) (j * 10 + id / 16));
                    u.setY((short) (id % 16));
                    u.setEMPHASIS("Minimize Losses");
                    u.setEXPERIENCE("veteran");
                    //u.setCHARACTERISTICS("4001");
                    u.setPROFICIENCY((short) 100);
                    u.setREADINESS((short) 100);
                    u.setSUPPLY((short) 100);
                    EQUIPMENT e = new EQUIPMENT();
                    u.getEQUIPMENT().add(e);
                    e.setID((byte) 1);
                    e.setNAME("Rifle Squad");
                    e.setNUMBER((short) 30);
                    e.setMAX((short) 30);
                }
            }
        }
        return game;
    }

    public static GAME generateTestUnitColors(File f) {

        ToawUnitIcon[] allToawUnitIcons = ToawUnitIcon.values();
        GAME game = (GAME) AresToolsIO.openToawFile(f);
        MAP map = generateTestMap(20, 32);
        game.setMAP(map);
        OOB oob = new OOB();
        game.setOOB(oob);
        for (short j = 0; j < 2; j++) {
            FORCE force = new FORCE();
            force.setID((byte) (j + 1));
            force.setNAME("FORCE " + j);
            force.setFlag(j);
            force.setFORMATIONUNASSIGNED(null);
            for (short k = 0; k < 4; k++) {
                FORMATION formation = new FORMATION();
                formation.setID((short) (k + 1));
                formation.setNAME("Formation " + (k + 1));
                force.getFORMATION().add(formation);
                oob.getFORCE().add(force);
                List<UNIT> units = formation.getUNIT();
                for (short i = 0; i < 32; i++) {
                    UNIT u = new FORMATION.UNIT();
                    units.add(u);
                    int id = k * 32 + i;
                    u.setID((short) (id + 1));
                    u.setNAME(allToawUnitIcons[id].getToawName());
                    u.setICON(allToawUnitIcons[id].getToawName());
                    u.setICONID((short) id);
                    u.setSIZE("Battalion");
                    if (id > 109) {
                        u.setCOLOR((short) 109);
                    } else {
                        u.setCOLOR((short) id);
                    }
                    u.setSTATUS(i);
                    u.setX((short) (j * 10 + id / 16));
                    u.setY((short) (id % 16));
                    u.setEMPHASIS("Minimize Losses");
                    u.setEXPERIENCE("veteran");
                    //u.setCHARACTERISTICS("4001");
                    u.setPROFICIENCY((short) 100);
                    u.setREADINESS((short) 100);
                    u.setSUPPLY((short) 100);
                    EQUIPMENT e = new EQUIPMENT();
                    u.getEQUIPMENT().add(e);
                    e.setID((byte) 1);
                    e.setNAME("Rifle Squad");
                    e.setNUMBER((short) 30);
                    e.setMAX((short) 30);
                }
            }
        }
        return game;
    }
}
