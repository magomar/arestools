package toaw.data;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public interface ToawEnumToAres<E extends Enum<E>> {

    public String getToawName();

    public E getAresEnum();
    
}
