package toaw.data;

import ares.data.wrappers.scenario.UnitType;

/**
 *
 * @author Mario Gomez
 */
public enum ToawUnitIcon implements ToawEnumToAres<UnitType> {

    HEADQUARTERS0("Headquarters", UnitType.HEADQUARTERS),
    HEADQUARTERS1("Headquarters", UnitType.HEADQUARTERS),
    KAMPFGRUPPE("Kampfgruppe", UnitType.KAMPFGRUPPE),
    TANK("Tank", UnitType.TANK),
    MECHANIZED("Mechanized", UnitType.MECHANIZED),
    ARMORED_CAVALRY("Armored Cavalry", UnitType.ARMORED_CAVALRY),
    ASSAULT_GUN("Assault Gun", UnitType.ASSAULT_GUN),
    ARMORED_ARTILLERY("Armored Artillery", UnitType.ARMORED_ARTILLERY),
    ARMORED_ANTITANK("Armored Antitank", UnitType.ARMORED_ANTITANK),
    ARMORED_RECON("Armored Recon", UnitType.ARMORED_RECON),
    INFANTRY("Infantry", UnitType.INFANTRY),
    INFANTRY_HVY_WPNS("Infantry Hvy Wpns", UnitType.INFANTRY_HVY_WPNS),
    MACHINE_GUN("Machine Gun", UnitType.MACHINE_GUN),
    CAVALRY("Cavalry", UnitType.CAVALRY),
    ANTITANK14("Antitank", UnitType.ANTITANK),
    ANTITANK15("Antitank", UnitType.ANTITANK),
    ARTILLERY("Artillery", UnitType.ARTILLERY),
    HORSE_ARTILLERY("Horse Artillery", UnitType.HORSE_ARTILLERY),
    INF_ARTILLERY("Inf Artillery", UnitType.INF_ARTILLERY),
    ROCKET_ARTILLERY("Rocket Artillery", UnitType.ROCKET_ARTILLERY),
    ANTI_AIRCRAFT("Anti Aircraft", UnitType.ANTI_AIRCRAFT),
    MOTOR_INFANTRY("Motor Infantry", UnitType.MOTOR_INFANTRY),
    MOTOR_HVY_WPNS("Motor Hvy Wpns", UnitType.MOTOR_HVY_WPNS),
    MOTOR_CAVALRY("Motor Cavalry", UnitType.MOTOR_CAVALRY),
    MOTORCYCLE("Motorcycle", UnitType.MOTORCYCLE),
    MOTOR_ANTITANK25("Motor Antitank", UnitType.MOTOR_ANTITANK),
    MOTOR_ANTITANK26("Motor Antitank", UnitType.MOTOR_ANTITANK),
    MOTOR_ARTILLERY("Motor Artillery", UnitType.MOTOR_ARTILLERY),
    MOTOR_ROCKET("Motor Rocket", UnitType.MOTOR_ROCKET),
    MOTOR_ANTI_AIR("Motor Anti Air", UnitType.MOTOR_ANTI_AIR),
    MOUNTAIN_INFANTRY("Mountain Infantry", UnitType.MOUNTAIN_INFANTRY),
    MOUNTAIN_HVY_WPNS("Mountain Hvy Wpns", UnitType.MOUNTAIN_HVY_WPNS),
    MOUNTAIN_CAV("Mountain Cav", UnitType.MOUNTAIN_CAV),
    MTN_CAV_HVY_WPNS("Mtn Cav Hvy Wpns", UnitType.MTN_CAV_HVY_WPNS),
    PARACHUTE("Parachute", UnitType.PARACHUTE),
    PARACHUTE_ANTITANK("Parachute Antitank", UnitType.PARACHUTE_ANTITANK),
    PARACHUTE_INFANTRY("Parachute Infantry", UnitType.PARACHUTE_INFANTRY),
    PARACHUTE_HVY_WPNS("Parachute Hvy Wpns", UnitType.PARACHUTE_HVY_WPNS),
    AIRBORNE_RECON("Airborne Recon", UnitType.AIRBORNE_RECON),
    AIRBORNE_ARTILLERY("Airborne Artillery", UnitType.AIRBORNE_ARTILLERY),
    AIR("Air", UnitType.AIR),
    FIGHTER41("Fighter", UnitType.FIGHTER),
    FIGHTER_BOMBER42("Fighter Bomber", UnitType.FIGHTER_BOMBER),
    LIGHT_BOMBER43("Light Bomber", UnitType.LIGHT_BOMBER),
    MEDIUM_BOMBER("Medium Bomber", UnitType.MEDIUM_BOMBER),
    HEAVY_BOMBER45("Heavy Bomber", UnitType.HEAVY_BOMBER),
    NAVAL_FIGHTER("Naval Fighter", UnitType.NAVAL_FIGHTER),
    NAVAL_ATTACK("Naval Attack", UnitType.NAVAL_ATTACK),
    NAVAL_BOMBER("Naval Bomber", UnitType.NAVAL_BOMBER),
    GLIDER_TANK("Glider Tank", UnitType.GLIDER_TANK),
    GLIDER_INFANTRY("Glider Infantry", UnitType.GLIDER_INFANTRY),
    GLIDER_HVY_WPNS("Glider Hvy Wpns", UnitType.GLIDER_HVY_WPNS),
    GLIDER_ARTILLERY("Glider Artillery", UnitType.GLIDER_ARTILLERY),
    GLIDER_RECON("Glider Recon", UnitType.GLIDER_RECON),
    GLIDER_ANTITANK("Glider Antitank", UnitType.GLIDER_ANTITANK),
    ENGINEER("Engineer", UnitType.ENGINEER),
    AIRBORNE_ENGINEER("Airborne Engineer", UnitType.AIRBORNE_ENGINEER),
    ARMORED_ENGINEER("Armored Engineer", UnitType.ARMORED_ENGINEER),
    AMPHIBIOUS_ARMOR("Amphibious Armor", UnitType.AMPHIBIOUS_ARMOR),
    SPECIAL_FORCES("Special Forces", UnitType.SPECIAL_FORCES),
    MARINE_INFANTRY("Marine Infantry", UnitType.MARINE_INFANTRY),
    CHEMICAL_ARTILLERY("Chemical Artillery", UnitType.CHEMICAL_ARTILLERY),
    COASTAL_ARTILLERY62("Coastal Artillery", UnitType.COASTAL_ARTILLERY),
    COASTAL_ARTILLERY63("Coastal Artillery", UnitType.COASTAL_ARTILLERY),
    AMPHIBIOUS("Amphibious", UnitType.AMPHIBIOUS),
    NAVAL_TASK_FORCE("Naval Task Force", UnitType.NAVAL_TASK_FORCE),
    FIGHTER66("Fighter", UnitType.FIGHTER),
    FIGHTER_BOMBER67("Fighter Bomber", UnitType.FIGHTER_BOMBER),
    LIGHT_BOMBER68("Light Bomber", UnitType.LIGHT_BOMBER),
    HEAVY_BOMBER69("Heavy Bomber", UnitType.HEAVY_BOMBER),
    JET_FIGHTER("Jet Fighter", UnitType.JET_FIGHTER),
    JET_BOMBER("Jet Bomber", UnitType.JET_BOMBER),
    JET_HEAVY_BOMBER("Jet Heavy Bomber", UnitType.JET_HEAVY_BOMBER),
    HEAVY_NAVAL("Heavy Naval", UnitType.HEAVY_NAVAL),
    MEDIUM_NAVAL("Medium Naval", UnitType.MEDIUM_NAVAL),
    LIGHT_NAVAL("Light Naval", UnitType.LIGHT_NAVAL),
    RIVERINE("Riverine", UnitType.RIVERINE),
    CARRIER_NAVAL("Carrier Naval", UnitType.CARRIER_NAVAL),
    ARMORED_TRAIN("Armored Train", UnitType.ARMORED_TRAIN),
    FIXED_ARTILLERY("Fixed Artillery", UnitType.FIXED_ARTILLERY),
    RAIL_ARTILLERY("Rail Artillery", UnitType.RAIL_ARTILLERY),
    MILITARY_POLICE("Military Police", UnitType.MILITARY_POLICE),
    TRANSPORT82("Transport", UnitType.TRANSPORT),
    IRREGULAR("Irregular", UnitType.IRREGULAR),
    FERRY_ENGINEER("Ferry Engineer", UnitType.FERRY_ENGINEER),
    AMPHIB_TRANSPORT("Amphib Transport", UnitType.AMPHIB_TRANSPORT),
    BICYCLE("Bicycle", UnitType.BICYCLE),
    SKI("Ski", UnitType.SKI),
    COMBAT_COMMAND_A("Combat Command A", UnitType.COMBAT_COMMAND_A),
    COMBAT_COMMAND_B("Combat Command B", UnitType.COMBAT_COMMAND_B),
    COMBAT_COMMAND_C("Combat Command C", UnitType.COMBAT_COMMAND_C),
    COMBAT_COMMAND_R("Combat Command R", UnitType.COMBAT_COMMAND_R),
    TASK_FORCE("Task Force", UnitType.TASK_FORCE),
    RAILROAD_REPAIR("Railroad Repair", UnitType.RAILROAD_REPAIR),
    HVY_ANTITANK("Hvy Antitank", UnitType.HVY_ANTITANK),
    BORDER("Border", UnitType.BORDER),
    BATTLEGROUP("Battlegroup", UnitType.BATTLEGROUP),
    SECURITY("Security", UnitType.SECURITY),
    HVY_ARMOR("Hvy Armor", UnitType.HVY_ARMOR),
    RESERVE("Reserve", UnitType.RESERVE),
    TRANSPORT100("Transport", UnitType.TRANSPORT),
    GARRISON("Garrison", UnitType.GARRISON),
    MOTOR_MACHINEGUN("Motor Machinegun", UnitType.MOTOR_MACHINEGUN),
    AMORED_HVY_ARTY("Amored Hvy Arty", UnitType.ARMORED_HVY_ARTY),
    MOTOR_ENGINEER("Motor Engineer", UnitType.MOTOR_ENGINEER),
    HVY_ARTILLERY("Hvy Artillery", UnitType.HVY_ARTILLERY),
    MORTAR("Mortar", UnitType.MORTAR),
    HVY_MORTAR("Hvy Mortar", UnitType.HVY_MORTAR),
    ATTACK_HELICOPTER("Attack Helicopter", UnitType.ATTACK_HELICOPTER),
    RECON_HELICOPTER("Recon Helicopter", UnitType.RECON_HELICOPTER),
    TRANS_HELICOPTER("Trans Helicopter", UnitType.TRANS_HELICOPTER),
    GUERRILLA("Guerrilla", UnitType.GUERRILLA),
    AIRMOBILE("Airmobile", UnitType.AIRMOBILE),
    AIRMOBILE_ANTITANK("Airmobile Antitank", UnitType.AIRMOBILE_ANTITANK),
    AIRMOBILE_INFANTRY("Airmobile Infantry", UnitType.AIRMOBILE_INFANTRY),
    AIRMOBILE_HVY_WPN("Airmobile Hvy Wpn", UnitType.AIRMOBILE_HVY_WPN),
    AIRMOBILE_CAVALRY("Airmobile Cavalry", UnitType.AIRMOBILE_CAVALRY),
    AIRMOBILE_ARTY("Airmobile Arty", UnitType.AIRMOBILE_ARTY),
    AIRMOBILE_ENGINEER("Airmobile Engineer", UnitType.AIRMOBILE_ENGINEER),
    AIRMOBILE_ANTI_AIR("Airmobile Anti Air", UnitType.AIRMOBILE_ANTI_AIR),
    PARACHUTE_ANTI_AIR("Parachute Anti Air", UnitType.PARACHUTE_ANTI_AIR),
    MISSILE_ARTILLERY("Missile Artillery", UnitType.MISSILE_ARTILLERY),
    CIVILIAN("Civilian", UnitType.CIVILIAN),
    SUPPLY("Supply", UnitType.SUPPLY),
    EMBARKED_HELI("(Embarked Heli)", UnitType.EMBARKED_HELI),
    EMBARKED_NAVAL("(Embarked Naval)", UnitType.EMBARKED_NAVAL),
    EMBARKED_RAIL("(Embarked Rail)", UnitType.EMBARKED_RAIL),
    EMBARKED_AIR("(Embarked Air)", UnitType.EMBARKED_AIR);
    
    private final String toawXMLName;
    private final UnitType aresEnum;

    private ToawUnitIcon(final String toawXMLName, final UnitType aresEnum) {
        this.toawXMLName = toawXMLName;
        this.aresEnum = aresEnum;
    }

    @Override
    public String getToawName() {
        return toawXMLName;
    }

    @Override
    public UnitType getAresEnum() {
        return aresEnum;
    }
//    public static ToawUnitIcon searchByName(String toawXMLName) {
//        for (ToawUnitIcon tui : values()) {
//            if (tui.getToawName().equalsIgnoreCase(toawXMLName)) {
//                return tui;
//            }
//        }
//        return null;
//    }
}
