package toaw.data;

import ares.data.wrappers.scenario.Availability;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum ToawStatus implements ToawEnumToAres<Availability> {

    NOT_DEPLOYED(Availability.NOT_DEPLOYED),
    TURN(Availability.TURN),
    EVENT(Availability.EVENT),
    DEFENDING,
    ENTRENCHED,
    FORTIFIED,
    TACTICAL_RESERVE,
    LOCAL_RESERVE,
    MOBILE,
    MOVING,
    ATTACKING,
    L_ATTACKING,
    RETREATED,
    ROUTED,
    ADVANCED,
    WITHDRAWN(Availability.WITHDRAWN),
    EXITED(Availability.EXITED),
    EMBARKED,
    ELIMINATED(Availability.ELIMINATED),
    UNKNOWN_19,
    UNKNOWN_20,
    EMBARKED_RAIL,
    EMBARKED_AIRBORNE,
    EMBARKED_NAVAL,
    DIVIDED(Availability.DIVIDED),
    NUKE,
    EMBARKED_BIS,
    BRIDGE_ATTACK,
    AIRFIELD_ATTACK,
    REORGANIZING;
    private final Availability availability;

    private ToawStatus(Availability availability) {
        this.availability = availability;
    }

    private ToawStatus() {
        this.availability = Availability.AVAILABLE;
    }

    @Override
    public String getToawName() {
        return name();
    }

    @Override
    public Availability getAresEnum() {
        return availability;
    }
}
