package toaw.data;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum ToawScale {

    SCALE_2_5_KM(2.5),
    SCALE_5_KM(5),
    SCALE_10_KM(10),
    SCALE_15_KM(15),
    SCALE_20(20),
    SCALE_25_KM(25),
    SCALE_50_KM(50);
    private final double km;

    private ToawScale(double km) {
        this.km = km;
    }

    public double getKm() {
        return km;
    }


}
