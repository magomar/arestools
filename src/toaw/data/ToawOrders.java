package toaw.data;

import ares.data.wrappers.scenario.Frontage;
import ares.data.wrappers.scenario.OperationalStance;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum ToawOrders implements ToawEnumToAres<OperationalStance> {

    ATTACK(OperationalStance.OFFENSIVE),
    SECURE(OperationalStance.OFFENSIVE, Frontage.WIDE),
    DELAY(OperationalStance.OFFENSIVE, true, false),
    WAIT(OperationalStance.OFFENSIVE, true, true),
    ADVANCE(OperationalStance.OFFENSIVE),
    DEFEND(OperationalStance.DEFENSIVE),
    SCREEN(OperationalStance.DEFENSIVE, Frontage.WIDE),
    STATIC(OperationalStance.DEFENSIVE, true, false),
    HOLD(OperationalStance.DEFENSIVE, true, true),
    FIXED(OperationalStance.FIXED),
    GARRISON(OperationalStance.GARRISON),
    INDEPENDENT(OperationalStance.SECURITY),
    MANUAL(OperationalStance.DEFENSIVE);
    /**
     * Main operational stance
     */
    private final OperationalStance stance;
    /**
     * Weather the formation needs activation. Formations that needsActivation do not acept orders until activated.
     * Activation may occur at prefixed dates, when units in the formation are attacked, or scripted events.
     */
    private final boolean needsActivation;
    /**
     * If true, the activation is automatic for the human player .In other words, activation restrictions only affect
     * the Programmed Opponent
     */
    private final boolean onlyPO;
    private Frontage frontage;

    private ToawOrders(final OperationalStance stance) {
        this(stance, Frontage.NORMAL, false, false);
    }

    private ToawOrders(final OperationalStance stance, final Frontage frontage) {
        this(stance, frontage, false, false);
    }

    private ToawOrders(final OperationalStance stance, final boolean needsActivation, final boolean onlyPO) {
        this(stance, Frontage.NORMAL, needsActivation, onlyPO);
    }

    private ToawOrders(final OperationalStance stance, final Frontage frontage, final boolean needsActivation, final boolean onlyPO) {
        this.stance = stance;
        this.frontage = frontage;
        this.needsActivation = needsActivation;
        this.onlyPO = onlyPO;
    }

    @Override
    public String getToawName() {
        return name();
    }

    @Override
    public OperationalStance getAresEnum() {
        return stance;
    }

    public OperationalStance getStance() {
        return stance;
    }

    public boolean needsActivation() {
        return needsActivation;
    }

    public boolean onlyPO() {
        return onlyPO;
    }

    public Frontage getFrontage() {
        return frontage;
    }
}
