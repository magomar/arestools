package toaw.data;

import ares.application.shared.gui.profiles.GraphicProperties;
import ares.application.shared.gui.profiles.NonProfiledGraphicProperty;
import ares.application.shared.gui.profiles.ProfiledGraphicProperty;
import ares.application.shared.gui.providers.ImageProvider;
import ares.application.shared.gui.providers.ProfiledImageProviderFactory;
import ares.application.shared.gui.providers.MatrixImageProvider;
import ares.data.wrappers.scenario.TerrainType;
import ares.platform.scenario.board.Direction;
import toaw.graphics.ToawGraphicsProfile;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum ToawTerrain implements ToawEnumToAres<TerrainType>, ProfiledImageProviderFactory {

//OPEN(false,TerrainType.OPEN,"misc","misc"),
    ARID(false, TerrainType.ARID, "arid", "arid"),
    SAND(false, TerrainType.SAND, "sandy", "sandy"),
    DUNES(false, TerrainType.DUNES, "r_sandy", "sandy"),
    BADLANDS(false, TerrainType.BADLANDS, "badlands", "null"),
    HILLS(false, TerrainType.HILLS, "hills", "hills"),
    MOUNTAINS(false, TerrainType.MOUNTAINS, "mountainous", "mountain"),
    ALPINE(false, TerrainType.ALPINE, "impassable", "mountain"),
    MARSH(false, TerrainType.MARSH, "marsh", "marsh"),
    FLOODED_MARSH(false, TerrainType.FLOODED_MARSH, "floodedmarsh", "marsh"),
    SHALLOW_WATER(false, TerrainType.SHALLOW_WATER, "s_water", "water"),
    DEEP_WATER(false, TerrainType.DEEP_WATER, "d_water", "deep_water"),
    CROPLANDS(false, TerrainType.CROPLANDS, "l_cultivated", "null"),
    BOCAGE_HEDGEROW(false, TerrainType.BOCAGE_HEDGEROW, "h_cultivated", "null"),
    URBAN(false, TerrainType.URBAN, "l_urban", "l_urban"),
    DENSE_URBAN(false, TerrainType.DENSE_URBAN, "h_urban", "h_urban"),
    URBAN_RUIN(false, TerrainType.URBAN_RUIN, "rl_urban", "l_urban"),
    DENSE_URBAN_RUIN(false, TerrainType.DENSE_URBAN_RUIN, "rh_urban", "h_urban"),
    ROCKY(false, TerrainType.ROCKY, "rocky", "null"),
    ESCARPMENT(true, TerrainType.ESCARPMENT, "escarpment", "null"),
    MAJOR_ESCARPMENT(true, TerrainType.MAJOR_ESCARPMENT, "major_escarpment", "null"),
    WADY(true, TerrainType.WADY, "dry_river", "null"),
    RIVER(true, TerrainType.RIVER, "river", "water"),
    SUPER_RIVER(true, TerrainType.SUPER_RIVER, "major_river", "water"),
    CANAL(true, TerrainType.CANAL, "canal", "water"),
    SUPER_CANAL(true, TerrainType.SUPER_CANAL, "major_canal", "water"),
    EVERGREEN_FOREST(false, TerrainType.EVERGREEN_FOREST, "c_forest", "null"),
    FOREST(false, TerrainType.FOREST, "d_forest", "null"),
    LIGHT_WOODS(false, TerrainType.LIGHT_WOODS, "M_forest", "null"),
    JUNGLE(false, TerrainType.JUNGLE, "t_forest", "null"),
    FORTIFICATION(true, TerrainType.FORTIFICATION, "fortifiedline", "null"),
    ROAD(true, TerrainType.ROAD, "road", "null"),
    IMPROVED_ROAD(true, TerrainType.IMPROVED_ROAD, "improvedroad", "null"),
    RAIL(true, TerrainType.RAIL, "railroad", "null"),
    BROKEN_RAIL(true, TerrainType.BROKEN_RAIL, "railroad_damaged_bridge", "null"),
    SHALLOW_WATER_DECORATOR(false, TerrainType.SHALLOW_WATER_DECORATOR, "s_water", "null"),
    DEEP_WATER_DECORATOR(false, TerrainType.DEEP_WATER_DECORATOR, "d_water", "null");
    ;
    private final boolean directional;
    private final TerrainType aresEnum;
    private final String filename;
    private final String microFilename;

    private ToawTerrain(final boolean directional, final TerrainType aresEnum,
            final String filename, String microFilename) {
        this.directional = directional;
        this.aresEnum = aresEnum;
        this.filename = "tiles_" + filename + ".png";
        this.microFilename = "tiles_" + microFilename + ".png";
    }

    public boolean isDirectional() {
        return directional;
    }

    @Override
    public String getToawName() {
        return name();
    }

    @Override
    public TerrainType getAresEnum() {
        return aresEnum;
    }

    public boolean isHasDir() {
        return directional;
    }

    /**
     * Obtains the image index in the image file, given a bitmask representing directions.<p>
     * There are 6 standard directions, so 2^6=64 combinations are possible. In addition, there is a special direction,
     * {@link Direction#C} that excludes any of the other directions. That makes 65 values to represent. However, the
     * absence of any direction is not represented (it is assumed bitMask is never 0), so we actually have 64 values,
     * numbered from 1 to 64.
     *
     * @see Direction
     * @param bitMask
     * @return
     */
    public static int getImageIndex(int bitMask) {
        return bitMask - 1;
    }

    @Override
    public String getFilename(int profile) {
        ToawGraphicsProfile tgp = ToawGraphicsProfile.values()[profile];
        if (profile == 0) {
            return tgp.getFilename(microFilename);
        } else {
            return tgp.getFilename(filename);
        }
    }

    @Override
    public ImageProvider createImageProvider(int profile) {
        int rows = GraphicProperties.getProperty(NonProfiledGraphicProperty.TILES_ROWS);
        int columns = GraphicProperties.getProperty(NonProfiledGraphicProperty.TILES_COLUMNS) + 2;
        int fullImageWidth = GraphicProperties.getProperty(ProfiledGraphicProperty.TILE_WIDTH, profile) * columns;
        int fullImageHeight = GraphicProperties.getProperty(ProfiledGraphicProperty.TILE_HEIGHT, profile) * rows;
        ToawGraphicsProfile tgp = ToawGraphicsProfile.values()[profile];
        return new MatrixImageProvider(tgp.getPath(), getFilename(profile), rows, columns, fullImageWidth, fullImageHeight);
    }
}
