package toaw.graphics;

import ares.application.shared.gui.profiles.GraphicProperties;
import ares.application.shared.gui.profiles.NonProfiledGraphicProperty;
import ares.application.shared.gui.profiles.ProfiledGraphicProperty;
import ares.platform.scenario.forces.UnitsColor;
import ares.application.shared.gui.providers.ImageProvider;
import ares.application.shared.gui.providers.ProfiledImageProviderFactory;
import ares.application.shared.gui.providers.MatrixImageProvider;
import java.awt.Point;
import toaw.data.ToawEnumToAres;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum ToawUnitsColor implements ToawEnumToAres<UnitsColor>, ProfiledImageProviderFactory {

    UNITS_BLUE_0("BLUE", "0", UnitsColor.UNITS_BLUE_0),
    UNITS_BLUE_1("BLUE", "1", UnitsColor.UNITS_BLUE_1),
    UNITS_BLUE_2("BLUE", "2", UnitsColor.UNITS_BLUE_2),
    UNITS_BLUE_3("BLUE", "3", UnitsColor.UNITS_BLUE_3),
    UNITS_BLUE_4("BLUE", "4", UnitsColor.UNITS_BLUE_4),
    UNITS_BROWN_0("BROWN", "0", UnitsColor.UNITS_BROWN_0),
    UNITS_BROWN_1("BROWN", "1", UnitsColor.UNITS_BROWN_1),
    UNITS_BROWN_2("BROWN", "2", UnitsColor.UNITS_BROWN_2),
    UNITS_BROWN_3("BROWN", "3", UnitsColor.UNITS_BROWN_3),
    UNITS_BROWN_4("BROWN", "4", UnitsColor.UNITS_BROWN_4),
    UNITS_GRAY_0("GRAY", "0", UnitsColor.UNITS_GRAY_0),
    UNITS_GRAY_1("GRAY", "1", UnitsColor.UNITS_GRAY_1),
    UNITS_GRAY_2("GRAY", "2", UnitsColor.UNITS_GRAY_2),
    UNITS_GRAY_3("GRAY", "3", UnitsColor.UNITS_GRAY_3),
    UNITS_GRAY_4("GRAY", "4", UnitsColor.UNITS_GRAY_4),
    UNITS_GREEN_0("GREEN", "0", UnitsColor.UNITS_GREEN_0),
    UNITS_GREEN_1("GREEN", "1", UnitsColor.UNITS_GREEN_1),
    UNITS_GREEN_2("GREEN", "2", UnitsColor.UNITS_GREEN_2),
    UNITS_GREEN_3("GREEN", "3", UnitsColor.UNITS_GREEN_3),
    UNITS_GREEN_4("GREEN", "4", UnitsColor.UNITS_GREEN_4),
    UNITS_RED_0("RED", "0", UnitsColor.UNITS_RED_0),
    UNITS_RED_1("RED", "1", UnitsColor.UNITS_RED_1),
    UNITS_RED_2("RED", "2", UnitsColor.UNITS_RED_2),
    UNITS_RED_3("RED", "3", UnitsColor.UNITS_RED_3),
    UNITS_RED_4("RED", "4", UnitsColor.UNITS_RED_4),
    UNITS_BLUELT_0("BLUELT", "0", UnitsColor.UNITS_BLUELT_0),
    UNITS_BLUELT_1("BLUELT", "1", UnitsColor.UNITS_BLUELT_1),
    UNITS_BLUELT_2("BLUELT", "2", UnitsColor.UNITS_BLUELT_2),
    UNITS_BLUELT_3("BLUELT", "3", UnitsColor.UNITS_BLUELT_3),
    UNITS_BLUELT_4("BLUELT", "4", UnitsColor.UNITS_BLUELT_4),
    UNITS_WHITE_0("WHITE", "0", UnitsColor.UNITS_WHITE_0),
    UNITS_WHITE_1("WHITE", "1", UnitsColor.UNITS_WHITE_1),
    UNITS_WHITE_2("WHITE", "2", UnitsColor.UNITS_WHITE_2),
    UNITS_WHITE_3("WHITE", "3", UnitsColor.UNITS_WHITE_3),
    UNITS_WHITE_4("WHITE", "4", UnitsColor.UNITS_WHITE_4),
    UNITS_YELLOW_0("YELLOW", "0", UnitsColor.UNITS_YELLOW_0),
    UNITS_YELLOW_1("YELLOW", "1", UnitsColor.UNITS_YELLOW_1),
    UNITS_YELLOW_2("YELLOW", "2", UnitsColor.UNITS_YELLOW_2),
    UNITS_YELLOW_3("YELLOW", "3", UnitsColor.UNITS_YELLOW_3),
    UNITS_YELLOW_4("YELLOW", "4", UnitsColor.UNITS_YELLOW_4),
    UNITS_TAN_0("TAN", "0", UnitsColor.UNITS_TAN_0),
    UNITS_TAN_1("TAN", "1", UnitsColor.UNITS_TAN_1),
    UNITS_TAN_2("TAN", "2", UnitsColor.UNITS_TAN_2),
    UNITS_TAN_3("TAN", "3", UnitsColor.UNITS_TAN_3),
    UNITS_TAN_4("TAN", "4", UnitsColor.UNITS_TAN_4),
    UNITS_GREENLT_0("GREENLT", "0", UnitsColor.UNITS_GREENLT_0),
    UNITS_GREENLT_1("GREENLT", "1", UnitsColor.UNITS_GREENLT_1),
    UNITS_GREENLT_2("GREENLT", "2", UnitsColor.UNITS_GREENLT_2),
    UNITS_GREENLT_3("GREENLT", "3", UnitsColor.UNITS_GREENLT_3),
    UNITS_GREENLT_4("GREENLT", "4", UnitsColor.UNITS_GREENLT_4),
    UNITS_GREENDK_0("GREENDK", "0", UnitsColor.UNITS_GREENDK_0),
    UNITS_GREENDK_1("GREENDK", "1", UnitsColor.UNITS_GREENDK_1),
    UNITS_GREENDK_2("GREENDK", "2", UnitsColor.UNITS_GREENDK_2),
    UNITS_GREENDK_3("GREENDK", "3", UnitsColor.UNITS_GREENDK_3),
    UNITS_GREENDK_4("GREENDK", "4", UnitsColor.UNITS_GREENDK_4),
    UNITS_012_0("012", "0", UnitsColor.UNITS_012_0),
    UNITS_012_1("012", "1", UnitsColor.UNITS_012_1),
    UNITS_012_2("012", "2", UnitsColor.UNITS_012_2),
    UNITS_012_3("012", "3", UnitsColor.UNITS_012_3),
    UNITS_012_4("012", "4", UnitsColor.UNITS_012_4),
    UNITS_013_0("013", "0", UnitsColor.UNITS_013_0),
    UNITS_013_1("013", "1", UnitsColor.UNITS_013_1),
    UNITS_013_2("013", "2", UnitsColor.UNITS_013_2),
    UNITS_013_3("013", "3", UnitsColor.UNITS_013_3),
    UNITS_013_4("013", "4", UnitsColor.UNITS_013_4),
    UNITS_014_0("014", "0", UnitsColor.UNITS_014_0),
    UNITS_014_1("014", "1", UnitsColor.UNITS_014_1),
    UNITS_014_2("014", "2", UnitsColor.UNITS_014_2),
    UNITS_014_3("014", "3", UnitsColor.UNITS_014_3),
    UNITS_014_4("014", "4", UnitsColor.UNITS_014_4),
    UNITS_015_0("015", "0", UnitsColor.UNITS_015_0),
    UNITS_015_1("015", "1", UnitsColor.UNITS_015_1),
    UNITS_015_2("015", "2", UnitsColor.UNITS_015_2),
    UNITS_015_3("015", "3", UnitsColor.UNITS_015_3),
    UNITS_015_4("015", "4", UnitsColor.UNITS_015_4),
    UNITS_016_0("016", "0", UnitsColor.UNITS_016_0),
    UNITS_016_1("016", "1", UnitsColor.UNITS_016_1),
    UNITS_016_2("016", "2", UnitsColor.UNITS_016_2),
    UNITS_016_3("016", "3", UnitsColor.UNITS_016_3),
    UNITS_016_4("016", "4", UnitsColor.UNITS_016_4),
    UNITS_017_0("017", "0", UnitsColor.UNITS_017_0),
    UNITS_017_1("017", "1", UnitsColor.UNITS_017_1),
    UNITS_017_2("017", "2", UnitsColor.UNITS_017_2),
    UNITS_017_3("017", "3", UnitsColor.UNITS_017_3),
    UNITS_017_4("017", "4", UnitsColor.UNITS_017_4),
    UNITS_018_0("018", "0", UnitsColor.UNITS_018_0),
    UNITS_018_1("018", "1", UnitsColor.UNITS_018_1),
    UNITS_018_2("018", "2", UnitsColor.UNITS_018_2),
    UNITS_018_3("018", "3", UnitsColor.UNITS_018_3),
    UNITS_018_4("018", "4", UnitsColor.UNITS_018_4),
    UNITS_019_0("019", "0", UnitsColor.UNITS_019_0),
    UNITS_019_1("019", "1", UnitsColor.UNITS_019_1),
    UNITS_019_2("019", "2", UnitsColor.UNITS_019_2),
    UNITS_019_3("019", "3", UnitsColor.UNITS_019_3),
    UNITS_019_4("019", "4", UnitsColor.UNITS_019_4),
    UNITS_020_0("020", "0", UnitsColor.UNITS_020_0),
    UNITS_020_1("020", "1", UnitsColor.UNITS_020_1),
    UNITS_020_2("020", "2", UnitsColor.UNITS_020_2),
    UNITS_020_3("020", "3", UnitsColor.UNITS_020_3),
    UNITS_020_4("020", "4", UnitsColor.UNITS_020_4),
    UNITS_021_0("021", "0", UnitsColor.UNITS_021_0),
    UNITS_021_1("021", "1", UnitsColor.UNITS_021_1),
    UNITS_021_2("021", "2", UnitsColor.UNITS_021_2),
    UNITS_021_3("021", "3", UnitsColor.UNITS_021_3),
    UNITS_021_4("021", "4", UnitsColor.UNITS_021_4),
    UNITS_022_0("022", "0", UnitsColor.UNITS_022_0),
    UNITS_022_1("022", "1", UnitsColor.UNITS_022_1),
    UNITS_022_2("022", "2", UnitsColor.UNITS_022_2),
    UNITS_022_3("022", "3", UnitsColor.UNITS_022_3),
    UNITS_022_4("022", "4", UnitsColor.UNITS_022_4);
    private final String filename;
    private final String microFilename;
    private final UnitsColor aresEnum;
    private final Point[] coordinatesByIndex;

    private ToawUnitsColor(String counterColor, String symbolColor, UnitsColor aresEnum) {
        this.filename = "units_1_" + counterColor + "_" + symbolColor + ".png";
        this.microFilename = "units_" + counterColor.toLowerCase() + ".png";
        this.aresEnum = aresEnum;
        int rows = GraphicProperties.getProperty(NonProfiledGraphicProperty.UNITS_ROWS);
        int columns = GraphicProperties.getProperty(NonProfiledGraphicProperty.UNITS_COLUMNS);
        int numIcons = rows * columns;
        coordinatesByIndex = new Point[numIcons];
        for (int i = 0; i < numIcons; i++) {
            int column = i / rows;
            int row = i % rows;
            coordinatesByIndex[i] = new Point(column, row);
        }
    }

    @Override
    public String getToawName() {
        return name();
    }

    @Override
    public UnitsColor getAresEnum() {
        return aresEnum;
    }

    @Override
    public String getFilename(int profile) {
        ToawGraphicsProfile tgp = ToawGraphicsProfile.values()[profile];
        if (profile == 0) {
            return tgp.getFilename(microFilename);
        } else {
            return tgp.getFilename(filename);
        }
    }

    @Override
    public ImageProvider createImageProvider(int profile) {
        int rows = GraphicProperties.getProperty(NonProfiledGraphicProperty.UNITS_ROWS);
        int columns = GraphicProperties.getProperty(NonProfiledGraphicProperty.UNITS_COLUMNS);
        int fullImageWidth = GraphicProperties.getProperty(ProfiledGraphicProperty.UNITS_WIDTH, profile);
        int fullImageHeight = GraphicProperties.getProperty(ProfiledGraphicProperty.UNITS_HEIGHT, profile);
        ToawGraphicsProfile tgp = ToawGraphicsProfile.values()[profile];
        return new MatrixImageProvider(tgp.getPath(), getFilename(profile), rows, columns, fullImageWidth, fullImageHeight);
    }

    public Point getCoordinates(int index) {
        return coordinatesByIndex[index];
    }
}
