package toaw.graphics;

import ares.application.shared.gui.profiles.GraphicProperties;
import ares.platform.io.ResourcePath;
import arestools.io.AresToolsIO;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 *
 * @author Mario Gómez Martínez <margomez at dsic.upv.es>
 */
public enum ToawGraphicsProfile  {

    MICRO() {
        @Override
        public String getFilename(String filename) {
            return "micro" + filename;
        }
    },
    MINI() {
        @Override
        public String getFilename(String filename) {
            return "s_" + filename;
        }
    },
    SMALL() {
        @Override
        public String getFilename(String filename) {
            return "s_" + filename;
        }
    },
    MEDIUM() {
        @Override
        public String getFilename(String filename) {
            return filename;
        }
    },
    HIGH() {
        @Override
        public String getFilename(String filename) {
            return "h_" + filename;
        }
    };
    private final String path;

    private ToawGraphicsProfile() {
        String subPath = GraphicProperties.getProfilePath(ordinal());
        Path folderPath = FileSystems.getDefault().getPath(AresToolsIO.TOAW_DATA_PATH, 
                ResourcePath.GRAPHICS.getRelativePath(),
                subPath);
        this.path = folderPath.toString();
    }
    public abstract String getFilename(String filename);

    public String getPath() {
        return path;
    }

    public int getOrdinal() {
        return ordinal();
    }
}
