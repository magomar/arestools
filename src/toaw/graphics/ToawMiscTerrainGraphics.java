package toaw.graphics;

import ares.application.shared.gui.profiles.GraphicProperties;
import ares.application.shared.gui.profiles.ProfiledGraphicProperty;
import ares.application.shared.gui.providers.AresMiscTerrainGraphics;
import ares.application.shared.gui.providers.ImageProvider;
import ares.application.shared.gui.providers.ProfiledImageProviderFactory;
import ares.application.shared.gui.providers.MatrixImageProvider;
import toaw.data.ToawEnumToAres;

/**
 *
 * @author Mario Gómez Martínez <margomez at dsic.upv.es>
 */
public enum ToawMiscTerrainGraphics implements ToawEnumToAres<AresMiscTerrainGraphics>, ProfiledImageProviderFactory {

    MISC(8, 10, "tiles_misc", "tiles_misc", AresMiscTerrainGraphics.TERRAIN_MISCELANEOUS),
    BORDER(6, 1, "Borders", "tiles_null", AresMiscTerrainGraphics.TERRAIN_BORDER),
    GRID(1, 1, "Hexoutline", "tiles_null", AresMiscTerrainGraphics.GRID),
//    BRASS_CURSOR(1, 1, "brass_cursor", "tiles_null", AresMiscTerrainGraphics.BRASS_CURSOR),
//    STEEL_CURSOR(8, 6, "steel_cursor", "tiles_null", AresMiscTerrainGraphics.STEEL_CURSOR),
//    RED_ARROWS(8, 6, "Movement_arrows", "tiles_null", AresMiscTerrainGraphics.RED_ARROWS),
    ;
    private final String filename;
    private final String microFilename;
    private final AresMiscTerrainGraphics aresEnum;
    private final int rows;
    private final int columns;

    private ToawMiscTerrainGraphics(final int rows, final int columns, final String filename, final String microFilename, final AresMiscTerrainGraphics aresGraphics) {
        this.rows = rows;
        this.columns = columns;
        this.filename = filename + ".png";
        this.microFilename = microFilename + ".png";
        this.aresEnum = aresGraphics;
    }

    @Override
    public String getToawName() {
        return name();
    }

    @Override
    public AresMiscTerrainGraphics getAresEnum() {
        return aresEnum;
    }

    @Override
    public String getFilename(int profile) {
        ToawGraphicsProfile tgp = ToawGraphicsProfile.values()[profile];
        if (profile == 0) {
            return tgp.getFilename(microFilename);
        } else {
            return tgp.getFilename(filename);
        }
    }

    @Override
    public ImageProvider createImageProvider(int profile) {
        int fullImageWidth = GraphicProperties.getProperty(ProfiledGraphicProperty.TILE_WIDTH, profile) * columns;
        int fullImageHeight = GraphicProperties.getProperty(ProfiledGraphicProperty.TILE_HEIGHT, profile) * rows;
        ToawGraphicsProfile tgp = ToawGraphicsProfile.values()[profile];
        return new MatrixImageProvider(tgp.getPath(), getFilename(profile), rows, columns, fullImageWidth, fullImageHeight);
    }
}
