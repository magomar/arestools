package helper;

import toaw.data.ToawTerrain;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class TestImageIndex {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        for (int bitMask = 1; bitMask < 65; bitMask++) {
            int imageIndex = ToawTerrain.getImageIndex(bitMask);
            System.out.println(bitMask + " -> " + imageIndex);
        }
    }
}
