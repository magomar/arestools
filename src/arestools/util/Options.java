package arestools.util;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class Options {

    public final static String[] SCENARIO_FOLDERS = {"Classic TOAW", "Spanish Civil War", "Test", "WW II - Asia",
        "WW II - East Front", "WW II - Mediterranean", "WW II - Misc", "WW II - West Front"};
}
