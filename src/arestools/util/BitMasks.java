package arestools.util;

import java.util.EnumSet;
import java.util.Set;

/**
 *
 * @author Mario Gómez Martínez <margomez at dsic.upv.es>
 */
public class BitMasks {

    public static <E extends Enum<E>> int convertFlagsToBitMask(E[] flags) {
        int mask = 0;
        for (E e : flags) {
            int bit = 1 << e.ordinal();
            mask |= bit;
        }
        return mask;
    }

    public static <E extends Enum<E>> int convertFlagsToBitMask(Set<E> flags) {
        int mask = 0;
        for (E e : flags) {
            int bit = 1 << e.ordinal();
            mask |= bit;
        }
        return mask;
    }

    public static <E extends Enum<E>> Set<E> convertBitMaskToFlags(int mask, Class<E> enumClass) {
        Set<E> flags = EnumSet.noneOf(enumClass);
        E[] allFlags = enumClass.getEnumConstants();
        for (int bit = 0; bit < allFlags.length; bit++) {
            if (testBitFlag(mask, bit)) {
                flags.add(allFlags[bit]);
            }
        }
        return flags;
    }

    public static boolean testBitFlag(int mask, int bit) {
        int flag = 1 << bit;
        boolean bitIsSet = (mask & flag) != 0;
        return bitIsSet;
    }
}
