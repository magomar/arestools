package arestools.io;

import ares.platform.io.AresFileType;
import ares.platform.io.FileIO;
import ares.platform.io.ResourcePath;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class BatchFileProcessor {

    private static final Logger LOG = Logger.getLogger(BatchFileProcessor.class.getName());

    public static int readOneScenarioFolder(String scenarioPath) {
        File folder = ResourcePath.SCENARIOS.getSubPath(scenarioPath).toFile();
        Collection<File> files = listFiles(folder, AresFileType.SCENARIO.getFileTypeFilter(), true);
        for (File file : files) {
            ares.data.wrappers.scenario.Scenario scen = FileIO.unmarshallJson(file, ares.data.wrappers.scenario.Scenario.class);
        }
        LOG.log(Level.INFO, "*** Files read from {0} = {1}", new Object[]{scenarioPath, files});
        return files.size();
    }

    public static int importOneScenarioFolderIntoJson(String scenarioPath) {
        Path sourcePath = AresToolsIO.getToawPath(ResourcePath.SCENARIOS.getRelativePath(), scenarioPath);
        Path targetPath = ResourcePath.SCENARIOS.getSubPath(scenarioPath);
        File folder = sourcePath.toFile();
        Collection<File> files = listFiles(folder, ToawFileType.GAME.getFileTypeFilter(), true);
        for (File file : files) {
            AresToolsIO.convertAndMarshallJson(file, targetPath.toString());
        }
        LOG.log(Level.INFO, "*** Files imported from {0} = {1}", new Object[]{scenarioPath, files});
        return files.size();
    }

    private static List<File> listFiles(File directory, FilenameFilter filter, boolean recurse) {
        List<File> files = new ArrayList<>();
        File[] entries = directory.listFiles();
        for (File entry : entries) {
            if (filter == null || filter.accept(directory, entry.getName())) {
                files.add(entry);
            }
            if (recurse && entry.isDirectory()) {
                files.addAll(listFiles(entry, filter, recurse));
            }
        }

        return files;
    }
}
