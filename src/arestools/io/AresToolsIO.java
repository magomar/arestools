package arestools.io;

import ares.data.wrappers.equipment.EquipmentDB;
import ares.data.wrappers.scenario.Map;
import ares.platform.io.AresFileType;
import ares.platform.io.FileIO;
import ares.platform.io.ResourcePath;
import arestools.gui.ToawAresGUI;
import arestools.tools.ImportToawScenario;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import toaw.data.jaxb.ACOWEXE;
import toaw.data.jaxb.GAME;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public final class AresToolsIO {

    private static final Logger LOG = Logger.getLogger(AresToolsIO.class.getName());
    private static final String JAXB_CONTEXT_PATH = "toaw.data.jaxb";
    private static final String JAXB_NAMESPACE = "toaw";
    private static JAXBContext JAXB_CONTEXT;
    private static Marshaller MARSHALLER;
    private static Unmarshaller UNMARSHALLER;
    public static final String TOAW_DATA_PATH = "ToawData";
    public static final String ARES_DATA_PATH = ".";

    static {
        try {
            JAXB_CONTEXT = JAXBContext.newInstance(JAXB_CONTEXT_PATH);
            UNMARSHALLER = JAXB_CONTEXT.createUnmarshaller();
            MARSHALLER = JAXB_CONTEXT.createMarshaller();
            MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        } catch (JAXBException ex) {
            Logger.getLogger(AresToolsIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Object openToawFile(File toawXMLFile) {
        String filePath = toawXMLFile.getAbsolutePath();
        String ext = filePath.substring(filePath.lastIndexOf('.'), filePath.length());
        ToawFileType fileType = ToawFileType.fromFileExtension(ext);
        Object source = null;
        switch (fileType.getFileExtension()) {
            case (".eqp"):
                source = FileIO.unmarshallXML(AresToolsIO.fixEquipmentFile(toawXMLFile), UNMARSHALLER, JAXB_NAMESPACE);
                break;
            case (".gam"):
                source = FileIO.unmarshallXML(toawXMLFile, UNMARSHALLER, JAXB_NAMESPACE);
                break;
            case (".mml"):
                source = FileIO.unmarshallXML(toawXMLFile, UNMARSHALLER, JAXB_NAMESPACE);
                Map m = ImportToawScenario.convert(((GAME) source).getMAP());
                ares.data.wrappers.scenario.Scenario g = new ares.data.wrappers.scenario.Scenario();
                g.setMap(m);
                break;
        }
        return source;
    }

    public static Object convertToawFile(File toawXMLFile) {
        LOG.log(Level.INFO, "Converting {0}", toawXMLFile.getName());
        String filePath = toawXMLFile.getAbsolutePath();
        String ext = filePath.substring(filePath.lastIndexOf('.'), filePath.length());
        ToawFileType fileType = ToawFileType.fromFileExtension(ext);
        Object source;
        Object target = null;
        switch (fileType.getFileExtension()) {
            case (".eqp"):
                source = FileIO.unmarshallXML(AresToolsIO.fixEquipmentFile(toawXMLFile), UNMARSHALLER, JAXB_NAMESPACE);
                target = ImportToawScenario.convert((ACOWEXE) source);
                break;
            case (".gam"):
                source = FileIO.unmarshallXML(toawXMLFile, UNMARSHALLER, JAXB_NAMESPACE);
                target = ImportToawScenario.convert((GAME) source);
                break;
            case (".mml"):
                source = FileIO.unmarshallXML(toawXMLFile, UNMARSHALLER, JAXB_NAMESPACE);
                Map m = ImportToawScenario.convert(((GAME) source).getMAP());
                ares.data.wrappers.scenario.Scenario g = new ares.data.wrappers.scenario.Scenario();
                g.setMap(m);
                target = g;
                break;
        }
        return target;
    }

    public static File convertAndMarshallJson(File toawXMLFile) {
        String aresFilename = convertToawToAresFileName(toawXMLFile);
        Object target = convertToawFile(toawXMLFile);
        LOG.log(Level.INFO, "Marshalling {0}", toawXMLFile.getName());
        File aresFile = FileIO.marshallJson(target, new File(aresFilename));
        return aresFile;
    }

    public static File convertAndMarshallJson(File toawXMLFile, String targetPath) {
        String aresFilename = convertToawToAresFileName(toawXMLFile, targetPath);
        Object target = convertToawFile(toawXMLFile);
        LOG.log(Level.INFO, "Marshalling {0}", toawXMLFile.getName());
        File aresFile = FileIO.marshallJson(target, new File(aresFilename));
        return aresFile;
    }

    public static File convertAndMarshallXML(File toawXMLFile, boolean useCompression, String targetPath) {
        String aresFilename = convertToawToAresFileName(toawXMLFile, targetPath);
        Object target = convertToawFile(toawXMLFile);
        LOG.log(Level.INFO, "Marshalling {0}", toawXMLFile.getName());
        File aresFile;
        if (useCompression) {
            //FILE_IO.marshallZipped(target, aresFilename);
            aresFile = FileIO.marshallGzipped(target, new File(aresFilename), MARSHALLER);
        } else {
            aresFile = FileIO.marshallXML(target, new File(aresFilename), MARSHALLER);
        }
        return aresFile;
    }

    public static File convertAndSerialize(File toawXMLFile, boolean compress, ResourcePath resourcePath) {
        String aresFilename = convertToawToAresFileName(toawXMLFile, resourcePath.getRelativePath());
        Object target = convertToawFile(toawXMLFile);
        LOG.log(Level.INFO, "Serializing {0}", toawXMLFile.getName());
        File aresFile;
        if (compress) {
            aresFile = serializeGZipped(target, aresFilename);
        } else {
            aresFile = serialize(target, aresFilename);
        }
        return aresFile;
    }

    public static File serialize(Object o, String filename) {
        File serFile = new File(filename + ".ser");
        try (FileOutputStream fos = new FileOutputStream(serFile)) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
                oos.writeObject(o);
                baos.writeTo(fos);
                return serFile;
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static File serializeGZipped(Object o, String filename) {
        File gzFile = new File(filename + ".gz");
        try (ObjectOutputStream out = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(gzFile)))) {
            out.writeObject(o);
            return gzFile;
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Object deserialize(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            try {
                return ois.readObject();
            } catch (ClassNotFoundException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }


        return null;
    }

    public static Object deserializeGZipped(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream(file)));
            try {
                return ois.readObject();
            } catch (ClassNotFoundException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static File fixEquipmentFile(File file) {
        File toawFixedEquipmentFile = null;
        try {
            //String newLine = System.getProperty("line.separator");
            toawFixedEquipmentFile = File.createTempFile("ToawEquipment", null);
            FileWriter writer = new FileWriter(toawFixedEquipmentFile);
            try (PrintWriter out = new PrintWriter(writer);
                    Scanner scanner = new Scanner(file)) {
                //            out.println("<ACOW_EXE xmlns='toaw'>"); //ad-hoc, bad solution
                //            scanner.skip("<ACOW_EXE>");
                while (scanner.hasNext()) {
                    String sourceString = scanner.nextLine().trim();
                    String targetString;
                    if (sourceString.startsWith("<ITEM")) {
                        targetString = "<ITEM>";
                    } else if (sourceString.startsWith("</ITEM")) {
                        targetString = "</ITEM>";
                    } else {
                        targetString = sourceString;
                    }
                    out.println(targetString);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ToawAresGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toawFixedEquipmentFile;
    }

    // Helper method just in case something has to be fixed
    public static void fixGame(File toawXMLFile) {
        throw new UnsupportedOperationException("Not supported yet.");
//        GAME game = (GAME) TOAW_IO.unmarshallXML(toawXMLFile);
//        GAME.OOB oob = game.getOOB();
//        for (GAME.OOB.FORCE force : oob.getFORCE()) {
//            for (GAME.OOB.FORCE.FORMATION fo : force.getFORMATION()) {
//                for (GAME.OOB.FORCE.FORMATION.UNIT un : fo.getUNIT()) {
//                    // Do something
//                }
//            }
//        }
//        TOAW_IO.marshallXML(game, toawXMLFile);

    }

    private static String convertToawToAresFileName(File toawFile, String targetPath) {
        String filename = getFileName(toawFile);
        String extension = getFileExtension(toawFile);
        ToawFileType toawFileType = ToawFileType.fromFileExtension(extension);
        AresFileType aresFileType = toawFileType.getAresFileType();
        Path path = FileSystems.getDefault().getPath(targetPath, filename + aresFileType.getFileExtension());
        return path.toString();
    }

    private static String convertToawToAresFileName(File toawFile) {
        String targetPath = getFilePath(toawFile);
        return convertToawToAresFileName(toawFile, targetPath);
    }

    private static String getFileName(File file) {
        String filename = file.getName();
        return filename.substring(0, filename.lastIndexOf('.'));
    }

    private static String getFileExtension(File file) {
        String filename = file.getName();
        return filename.substring(filename.lastIndexOf('.'), filename.length());
    }

    private static String getFilePath(File file) {
        String filename = file.getAbsolutePath();
        String separator = FileSystems.getDefault().getSeparator();
        return filename.substring(0, filename.lastIndexOf(separator));
    }

//    public static GAME openToawScenario(String... relativePath) {
//        File scenariofile = getToawPath(relativePath).toFile();
//        GAME game = FileIO.unmarshallJson(scenariofile, toaw.data.jaxb.GAME.class);
//        LOG.log(Level.INFO, "Scenario opened: {0}", scenariofile.getName());
//        return game;
//    }
//
//    public static ares.scenario.Scenario loadAresScenario(EquipmentDB eqp, String... relativePath) {
//        File scenariofile = getAresPath(relativePath).toFile();
//        ares.data.jaxb.Scenario scen = FileIO.unmarshallJson(scenariofile, ares.data.jaxb.Scenario.class);
//        ares.scenario.Scenario scenario = new ares.scenario.Scenario(scen, eqp);
//        LOG.log(Level.INFO, "Scenario loaded: {0}", scenariofile.getName());
//        return scenario;
//    }
//
//    public static EquipmentDB loadAresEquipment() {
//        Path equipmentPath = getAresPath(ResourcePath.EQUIPMENT.getRelativePath());
//        File equipmentFile = FileSystems.getDefault().getPath(equipmentPath.toString(), "ToawEquipment.equipment").toFile();
//        EquipmentDB eqp = FileIO.unmarshallJson(equipmentFile, EquipmentDB.class);
//        LOG.log(Level.INFO, "Equipment database opened: {0}", equipmentFile.getName());
//        return eqp;
//    }
    public static EquipmentDB loadAresEquipment() {
        File equipmentFile = ResourcePath.EQUIPMENT.getFile("ToawEquipment.equipment");
        EquipmentDB eqp = FileIO.unmarshallJson(equipmentFile, EquipmentDB.class);
        LOG.log(Level.INFO, "Equipment database opened: {0}", equipmentFile.getName());
        return eqp;
    }

    public static Path getToawPath(String... relativePath) {
        Path toawRelativePath = FileSystems.getDefault().getPath(TOAW_DATA_PATH, relativePath);
        return FileSystems.getDefault().getPath(System.getProperty("user.dir"), toawRelativePath.toString());
    }

    public static Path getAresPath(String... relativePath) {
        String separator = FileSystems.getDefault().getSeparator();
        StringBuilder sb = new StringBuilder(relativePath[0]);
        for (int i = 1; i < relativePath.length; i++) {
            sb.append(separator).append(relativePath[i]);
        }
        return FileSystems.getDefault().getPath(ARES_DATA_PATH, relativePath);
    }
}
