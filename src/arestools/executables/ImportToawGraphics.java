package arestools.executables;

import arestools.tools.ImportGraphics;
import toaw.graphics.ToawGraphicsProfile;

/**
 *
 * @author Mario Gómez Martínez <margomez at dsic.upv.es>
 */
public class ImportToawGraphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ToawGraphicsProfile[] profiles = ToawGraphicsProfile.values();
        for (int i = 0; i < profiles.length; i++) {
            ToawGraphicsProfile p = profiles[i];
            ImportGraphics.convertToawMiscGraphics(p);
            ImportGraphics.convertTerrainGraphics(p);
            ImportGraphics.convertUnitsGraphics(p);
            if (i != 0) {
                ImportGraphics.convertBorderGraphics(p);
            }
        }
    }
}
