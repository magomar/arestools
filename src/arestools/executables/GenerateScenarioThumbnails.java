package arestools.executables;

import ares.application.shared.gui.profiles.GraphicsModel;
import ares.application.shared.gui.providers.AresMiscTerrainGraphics;
import ares.platform.scenario.forces.UnitsColor;
import ares.data.wrappers.equipment.EquipmentDB;
import ares.platform.io.AresFileType;
import ares.platform.io.FileIO;
import ares.platform.io.ResourcePath;
import ares.platform.scenario.board.Terrain;
import ares.platform.scenario.forces.Force;
import ares.platform.scenario.forces.Formation;
import arestools.io.AresToolsIO;
import arestools.tools.CreateScenarioThumbnail;
import arestools.util.Stopwatch;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mario Gómez Martínez <margomez at dsic.upv.es>
 */
public class GenerateScenarioThumbnails {

    private final static String[] SCENARIO_FOLDERS = {"Classic TOAW"
//           , "Spanish Civil War", "Test", "WW II - Asia",
//        "WW II - East Front", "WW II - Mediterranean", "WW II - Misc", "WW II - West Front"
    };
    private static final Logger LOG = Logger.getLogger(GenerateScenarioThumbnails.class.getName());

    public static int loadScenarios(String scenarioPath, EquipmentDB eqp) {
        Stopwatch sw = new Stopwatch();
        sw.start();
        GraphicsModel.INSTANCE.addProfiledImageProviders(Terrain.values());
        GraphicsModel.INSTANCE.addProfiledImageProviders(AresMiscTerrainGraphics.values());
        GraphicsModel.INSTANCE.addProfiledImageProviders(UnitsColor.values());

        Path sourcePath = AresToolsIO.getAresPath(ResourcePath.SCENARIOS.getRelativePath(), scenarioPath);
        File folder = sourcePath.toFile();
        Collection<File> files = listFiles(folder, AresFileType.SCENARIO.getFileTypeFilter(), true);
        for (File file : files) {
            ares.data.wrappers.scenario.Scenario scen = FileIO.unmarshallJson(file, ares.data.wrappers.scenario.Scenario.class);
            ares.platform.scenario.Scenario scenario = new ares.platform.scenario.Scenario(scen, eqp);
            for (Force force : scenario.getForces()) {
                for (Formation formation : force.getFormations()) {
                    formation.initialize();
                }
            }
            GraphicsModel.INSTANCE.initialize(scenario.getBoard());
            BufferedImage image = CreateScenarioThumbnail.createScenarioThumbnail(scenario);
            String imageName = file.getPath().replace(AresFileType.SCENARIO.getFileExtension(), ".jpg");
            File imageFile = new File(imageName);
            FileIO.saveImage(image, imageFile, "jpg");
        }
        return files.size();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Stopwatch sw = new Stopwatch();
        int totalFiles = 0;
        EquipmentDB eqp = AresToolsIO.loadAresEquipment();
        sw.start();
        for (String scenarioPath : SCENARIO_FOLDERS) {
            int files = loadScenarios(scenarioPath, eqp);
            LOG.log(Level.INFO, "*** Files loaded from {0} = {1}", new Object[]{scenarioPath, files});
            totalFiles += files;
        }

        sw.stop();
        LOG.log(Level.INFO, "****** Total number of files loaded = {0}", totalFiles);
        LOG.log(Level.INFO, sw.toString());

    }

    public static List<File> listFiles(File directory, FilenameFilter filter, boolean recurse) {
        List<File> files = new ArrayList<>();
        File[] entries = directory.listFiles();
        for (File entry : entries) {
            if (filter == null || filter.accept(directory, entry.getName())) {
                files.add(entry);
            }
            if (recurse && entry.isDirectory()) {
                files.addAll(listFiles(entry, filter, recurse));
            }
        }

        return files;
    }
}
