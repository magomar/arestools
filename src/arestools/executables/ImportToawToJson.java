package arestools.executables;

import arestools.io.BatchFileProcessor;
import arestools.util.Options;
import arestools.util.Stopwatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ImportToawToJson {

    private static final Logger LOG = Logger.getLogger(ImportToawToJson.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Stopwatch sw = new Stopwatch();
        int totalFiles = 0;
        sw.start();
        for (String scenarioPath : Options.SCENARIO_FOLDERS) {
            int files = BatchFileProcessor.importOneScenarioFolderIntoJson(scenarioPath);
            totalFiles += files;
        }
        sw.stop();
        LOG.log(Level.INFO, "****** Total number of files imported = {0}", totalFiles);
        LOG.log(Level.INFO, sw.toString());

    }
}
