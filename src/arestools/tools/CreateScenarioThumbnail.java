package arestools.tools;

import ares.application.shared.boundaries.viewers.layerviewers.ImageLayerViewer;
import ares.application.player.views.PlayerBoardView;
import ares.application.shared.boundaries.viewers.layerviewers.GridLayerViewer;
import ares.application.shared.boundaries.viewers.layerviewers.TerrainLayerViewer;
import ares.application.shared.boundaries.viewers.layerviewers.UnitsLayerViewer;
import ares.application.shared.gui.views.layerviews.GridLayerView;
import ares.application.shared.gui.views.layerviews.TerrainLayerView;
import ares.application.shared.gui.views.layerviews.UnitsLayerView;
import ares.platform.model.UserRole;
import ares.platform.scenario.Scenario;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class CreateScenarioThumbnail {

    private static final double MAX_SIZE = 500;

    public static BufferedImage createScenarioThumbnail(Scenario scenario) {
        PlayerBoardView boardView = new PlayerBoardView();
        TerrainLayerView terrainLayerView = (TerrainLayerView) boardView.getLayerView(TerrainLayerViewer.NAME);
        UnitsLayerView unitsLayerView = (UnitsLayerView) boardView.getLayerView(UnitsLayerViewer.NAME);
        // Load scenario
        boardView.setProfile(2);
        // Render board: paint terrain and units
        terrainLayerView.updateScenario(scenario.getModel(UserRole.GOD));
        unitsLayerView.updateScenario(scenario.getModel(UserRole.GOD));
        BufferedImage terrainImage = terrainLayerView.getGlobalImage();
        BufferedImage unitsImage = unitsLayerView.getGlobalImage();
        int width = terrainImage.getWidth();
        int height = terrainImage.getHeight();
        double ratio;
        if (width > height) {
            ratio = MAX_SIZE / width;
        } else {
            ratio = MAX_SIZE / height;
        }
        width = (int) (width * ratio);
        height = (int) (height * ratio);
        BufferedImage combined = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // paint both images preserving the alpha channels
        Graphics2D g = (Graphics2D) combined.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(terrainImage, 0, 0, width, height, null);
        g.drawImage(unitsImage, 0, 0, width, height, null);
        return combined;
    }
}
