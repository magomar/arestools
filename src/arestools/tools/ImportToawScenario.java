package arestools.tools;

import ares.data.wrappers.scenario.Replacements;
import ares.data.wrappers.scenario.Scenario;
import ares.data.wrappers.scenario.Availability;
import ares.data.wrappers.scenario.Trait;
import ares.data.wrappers.scenario.Variables;
import ares.data.wrappers.scenario.TerrainFeature;
import ares.data.wrappers.scenario.Terrain;
import ares.data.wrappers.scenario.Supplies;
import ares.data.wrappers.scenario.Track;
import ares.data.wrappers.scenario.SupportScope;
import ares.data.wrappers.scenario.Echelon;
import ares.data.wrappers.scenario.OOB;
import ares.data.wrappers.scenario.Events;
import ares.data.wrappers.scenario.Experience;
import ares.data.wrappers.scenario.Objective;
import ares.data.wrappers.scenario.UnitType;
import ares.data.wrappers.scenario.Force;
import ares.data.wrappers.scenario.Direction;
import ares.data.wrappers.scenario.Frontage;
import ares.data.wrappers.scenario.TurnLength;
import ares.data.wrappers.scenario.Environment;
import ares.data.wrappers.scenario.Unit;
import ares.data.wrappers.equipment.EquipmentDB;
import ares.data.wrappers.scenario.ForceVariables;
import ares.data.wrappers.scenario.Header;
import ares.data.wrappers.scenario.OperationalStance;
import ares.data.wrappers.scenario.MultiDirection;
import ares.data.wrappers.scenario.Emphasis;
import ares.data.wrappers.scenario.OpState;
import ares.data.wrappers.scenario.Orders;
import ares.data.wrappers.scenario.Formation;
import ares.data.wrappers.scenario.Place;
import ares.data.wrappers.scenario.TerrainType;
import ares.data.wrappers.scenario.Cell;
import ares.data.wrappers.scenario.Calendar;
import ares.data.wrappers.equipment.EquipmentDB.EquipmentCategory;
import ares.data.wrappers.scenario.Map;
import ares.data.wrappers.scenario.Unit.Equipment;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import toaw.data.ToawOrders;
import toaw.data.ToawScale;
import toaw.data.ToawStatus;
import toaw.data.ToawTerrain;
import toaw.data.ToawUnitIcon;
import toaw.data.jaxb.ACOWEXE;
import toaw.data.jaxb.GAME;
import toaw.data.jaxb.GAME.OOB.FORCE;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION.OBJECTIVES;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION.OBJECTIVES.OBJECTIVE;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION.UNIT;
import toaw.data.jaxb.GAME.OOB.FORCE.FORMATION.UNIT.EQUIPMENT;

/**
 *
 * @author Mario
 */
public class ImportToawScenario {

    static String scenario;
    final static Trait[] TRAITS = Trait.values();
    final static TerrainType[] TERRAIN_TYPES = TerrainType.values();
    final static TerrainFeature[] TERRAIN_FEATURES = TerrainFeature.values();
    final static Direction[] DIRECTIONS = Direction.values();
    final static MultiDirection[] MULTI_DIRECTIONS = MultiDirection.values();
    final static UnitType[] UNIT_TYPES = UnitType.values();
    final static Echelon[] ECHELONS = Echelon.values();
    final static ToawTerrain[] TOAW_TERRAINS = ToawTerrain.values();
    final static ToawUnitIcon[] TOAW_UNIT_ICONS = ToawUnitIcon.values();
    final static ToawScale[] TOAW_SCALES = ToawScale.values();
    final static ToawStatus[] TOAW_STATUS = ToawStatus.values();
    static final Set<Trait> AIRCRAFT = EnumSet.of(Trait.LOW_ALTITUDE_AIRCRAFT,
            Trait.HIGH_ALTITUDE_AIRCRAFT, Trait.NAVAL_AIRCRAFT);
    private static final Logger LOG = Logger.getLogger(ImportToawScenario.class.getName());

    public static EquipmentDB convert(ACOWEXE acowexe) {

        EquipmentDB ae = new EquipmentDB();
        List<EquipmentCategory> categories = ae.getEquipmentCategory();
        EquipmentCategory ec = null;
        int categoryId = 0;
        int eqpID = 0;

        List<ACOWEXE.UNITS.UNITSDATABASE.ITEM> items = acowexe.getUNITS().getUNITSDATABASE().getITEM();
        for (ACOWEXE.UNITS.UNITSDATABASE.ITEM i : items) {
            if (i.getFLAG0() == 1) { //Is a category, not an item
                ec = new EquipmentCategory();
                ec.setId(categoryId++);
                ec.setName(i.getNAME());
                ec.setCountry(i.getCOUNTRY());
                categories.add(ec);
            } else {
                EquipmentCategory.Item it = new EquipmentCategory.Item();
                ec.getItem().add(it);
                it.setId(eqpID++);
                it.setName(i.getNAME());
                it.setCountry(i.getCOUNTRY());
                int[] flags = {i.getFLAG0(), i.getFLAG1(), i.getFLAG2(),
                    i.getFLAG3(), i.getFLAG4(), i.getFLAG5(),
                    i.getFLAG6(), i.getFLAG7()};
                Set<Trait> traits = convertToawFlagsToAresTraits(flags);
                it.getTrait().addAll(traits);
                it.setAA(i.getAA());
                it.setAT(i.getAT());
                int gameAP = i.getAP();
                int gameDF = i.getDF();

                if (traits.contains(Trait.ARMORED)) {
                    gameAP = (i.getAP() / 8);
                    gameDF = 5 + i.getARMOR() / 10;
                } else if (isAircraft(traits)) {
                    gameAP = (i.getAP() / 2);
                }
                it.setAP(gameAP);
                it.setDF(gameDF);
                it.setArmor(i.getARMOR());
                it.setArtyRange(i.getARTYRNG());
                it.setEarlyRange(i.getEARLYRNG());
                it.setShellWeight(i.getSHELLW());
                it.setSAMRange(i.getSAMRNG());
                it.setNuke((int) i.getNUKE());
                it.setVolume(i.getVOL());
                it.setWeight((int) i.getWEIGHT());
                it.setIcon(i.getEQUIP1());

            }
        }
        return ae;
    }

    public static Header convert(GAME.HEADER header) {
        Header h = new Header();
        h.setName(header.getName());
        h.setDescription(header.getDescription());
        return h;
    }

    public static Calendar convert(GAME.CALENDAR cal) {
        Calendar c = new Calendar();
        TurnLength[] allTurnLengths = TurnLength.class.getEnumConstants();
        c.setTurnLength(allTurnLengths[cal.getTurnLength()]);

        GregorianCalendar gc = new GregorianCalendar(cal.getStartYear(), cal.getStartMonth(), cal.getStartDay());
        gc.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        XMLGregorianCalendar date = null;
        try {
            date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        } catch (DatatypeConfigurationException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        c.setStartDate(date);
        c.setStartQuarterDay(cal.getStartQuarterDay());
        c.setCurrentTurn(cal.getCurrentTurn());
        c.setFinalTurn(cal.getFinalTurn());
        return c;
    }

    public static Environment convert(GAME.ENVIRONMENT env) {
        Environment e = new Environment();

        e.setScale(TOAW_SCALES[env.getScale()].getKm());
        // TODO convert/adapt weather zones
        return e;
    }

    public static ForceVariables convert(GAME.FORCEVARIABLES forceVar) {
        ForceVariables fv = new ForceVariables();
        // TODO convert force variables
        return fv;
    }

    public static Variables convert(GAME.VARIABLES var) {
        Variables v = new Variables();
        // TODO convert variables
        return v;
    }

    public static Map convert(GAME.MAP map) {
        Map m = new Map();
        m.setMaxX(map.getMaxx());
        m.setMaxY(map.getMaxy());

        List<Cell> cells = m.getCell();
        for (GAME.MAP.CELL cell : map.getCELL()) {
            Cell c = new Cell();
            cells.add(c);
            String loc = cell.getLoc();
            String[] locStr = loc.split(",");
            int x = Integer.parseInt(locStr[0]);
            int y = Integer.parseInt(locStr[1]);
            c.setX(x);
            c.setY(y);
            String[] b = cell.getB().substring(1).split("/");
            List<Terrain> terrains = c.getTerrain();
            for (int i = 1; i < 37; i++) {
                int bitmask = Integer.parseInt(b[i]);
                if (bitmask > 0) {
                    Terrain t = new Terrain();
                    terrains.add(t);
                    ToawTerrain toawt = TOAW_TERRAINS[i-1];
                    t.setType(toawt.getAresEnum());
                    t.setDir(MULTI_DIRECTIONS[bitmask-1]);
                }
            }
            // entrechment
            int bitmask = Integer.parseInt(b[37]);
            if (bitmask > 0) {
                c.setEntrenchment(bitmask);
            }
            //miscelaneous flags
            List<TerrainFeature> features = c.getFeature();
            bitmask = Integer.parseInt(b[38]);
            if (bitmask > 0) {
                for (int j = 0; j < 7; j++) {
                    if (testBitFlag(bitmask, j)) {
                        features.add(TERRAIN_FEATURES[j]);
                    }
                }
            }

            // Ownership
            bitmask = Integer.parseInt(b[39]);
            if (testBitFlag(bitmask, 0)) {
                c.setOwner(1);
            } else {
                c.setOwner(0);
            }
//            if (testBitFlag(mask, 3)) {
//                System.out.println("Frozen (" + cell.getLoc() + "): " + mask);
//            }
//            else if (bitmask > 1) { // do not know, but happens in some scenarios
//                System.out.println("Flag 39 (" + cell.getLoc() + "): " + bitmask);
//            }

//            bitmask = Integer.parseInt(b[40]);
//            if (bitmask>0)  
//                System.out.println("Flag 40 (" + cell.getLoc() + "): " + bitmask);

            // Exclussion zones
            bitmask = Integer.parseInt(b[41]);
            if (testBitFlag(bitmask, 3)) {
                features.add(TerrainFeature.EXCLUDED_1);
            }
            if (testBitFlag(bitmask, 4)) {
                features.add(TerrainFeature.EXCLUDED_2);
            }

//            bitmask = Integer.parseInt(b[42]);
//            if (bitmask>0) System.out.println("Flag 42 (" + cell.getLoc() + "): " + bitmask);

            // Border (frontiers)
            bitmask = Integer.parseInt(b[43]);
            if (bitmask > 0) {
                Terrain t = new Terrain();
                terrains.add(t);
                t.setType(TerrainType.BORDER);
                t.setDir(MULTI_DIRECTIONS[bitmask-1]);
            }
            // Victory points
            bitmask = Integer.parseInt(b[44]);
            if (bitmask > 0) {
                c.setVP(bitmask);
            }

            // distance
            bitmask = Integer.parseInt(b[45]);
            if (bitmask > 0) {
                c.setDistance(bitmask);
            }

        }
        List<Place> pl = m.getPlace();
        for (GAME.MAP.PLACE place : map.getPLACE()) {
            Place p = new Place();
            pl.add(p);
            p.setId(place.getID());
            p.setName(place.getName());
            String[] locStr = place.getLoc().split(",");
            p.setX(Integer.parseInt(locStr[0]));
            p.setY(Integer.parseInt(locStr[1]));
        }
        return m;
    }

    static boolean checkUnit(UNIT un) {
        boolean ok = true;
        int id = un.getID();
        String name = un.getNAME();
        StringBuilder msg = new StringBuilder(scenario + "-> Skip Unit " + name + "(" + id + "): ");
        Availability availability = TOAW_STATUS[un.getSTATUS()].getAresEnum();
        String toawIcon = (un.getICONID() != null ? un.getICON() + un.getICONID() : un.getICON());
        if (availability != Availability.DIVIDED && "".equals(toawIcon)) {
            msg.append("Icon not found =").append(un.getICON());
            ok = false;
        }
        if (un.getX() == null && un.getGOINGTOX() == null) {
            msg.append("No coordinates");
            ok = false;
        }

//        int lastTurn = game.getCALENDAR().getFinalTurn();
//        List<EVENT> events = game.getEVENTS().getEVENT();
//        int lastEventID = events.get(events.size() - 1).getID();
//        if (status == Availability.TURN && un.getENTRY() != null && un.getENTRY() > lastTurn) {
//            msg.append("Stat=" + status + ", Entry=" + un.getENTRY() + ", Last turn=" + lastTurn + " X= ");
//            ok = false;
//        }
//        if (status == Availability.EVENT && un.getENTRY() != null && un.getENTRY() > lastEventID) {
//            msg.append("Availability=" + status + ", Entry event=" + un.getENTRY() + ", Last event=" + lastEventID+ " X= ");
//            ok = false;
//        }
        if (!ok) {
            System.err.println(msg);
        }
        return ok;
    }

    public static OOB convert(GAME.OOB oob) {
        OOB o = new OOB();
        for (GAME.OOB.FORCE force : oob.getFORCE()) {
            Force forc = new Force();
            o.getForce().add(forc);
            forc.setId(force.getID() - 1);
            forc.setName(force.getNAME());
            forc.setProficiency(force.getProficiency());
            forc.setSupply(force.getSupply());
            forc.setFlag(force.getFlag());
            List<Formation> formations = forc.getFormation();
            Formation root = new Formation();
            formations.add(root);
            root.setId(0);
            root.setName(forc.getName());
            root.setProficiency(forc.getProficiency());
            root.setSupply(forc.getSupply());
            root.setEchelon(Echelon.REGION);
            root.setCommander("General Commander");
            root.setDetails("");
            Orders ord = new Orders();
            ord.setOperationalStance(OperationalStance.SECURITY);
            ord.setEmphasis(Emphasis.MINIMIZE_LOSSES);
            ord.setSupportscope(SupportScope.ARMY_SUPPORT);
            ord.setFrontage(Frontage.NORMAL);
            ord.setOnlyPO(true);
            root.setOrders(ord);
            for (FORMATION fo : force.getFORMATION()) {
                Formation f = new Formation();
                formations.add(f);
                f.setId(fo.getID());
                f.setName(fo.getNAME());
                f.setParent(0);
                f.setProficiency(fo.getPROFICIENCY());
                f.setSupply(fo.getSUPPLY());
                f.setOrders(convertOrders(fo));
                List<Unit> units = f.getUnit();
                for (UNIT un : fo.getUNIT()) {
                    if (checkUnit(un)) {
                        Unit u = new Unit();
                        units.add(u);
                        u.setId(un.getID());
                        String name = un.getNAME();
                        u.setName(name);
                        ToawStatus status = TOAW_STATUS[un.getSTATUS()];
                        if (status == ToawStatus.MOBILE) {
                            u.setOpState(OpState.MOBILE);
                        } else {
                            u.setOpState(OpState.DEPLOYED);
                        }
                        Availability availability = status.getAresEnum();
                        u.setAvailability(availability);
                        if (availability != Availability.DIVIDED) {
                            String toawIcon = (un.getICONID() != null ? un.getICON() + un.getICONID() : un.getICON());
                            ToawUnitIcon tui = convertStringToEnum(ToawUnitIcon.class, toawIcon);
                            u.setType(tui.getAresEnum());
                            u.setIconId(transposeIconID(tui.ordinal()));
                            if (availability != Availability.TURN && availability != Availability.EVENT) {
                                u.setX(un.getX());
                                u.setY(un.getY());
                            } else {
                                u.setX(un.getGOINGTOX());
                                u.setY(un.getGOINGTOY());
                                if (un.getENTRY() != null) {
                                    u.setEntry((int) un.getENTRY());
                                } else {
                                    u.setEntry(1);
                                }
                            }
                        } else { // availability = DIVIDED
//                            System.out.println("(" +  u.getId() + ") " + name + " is divided ");
                            u.setType(UnitType.UNDEFINED);
                            u.setIconId(-1);
                        }
                        u.setColor(un.getCOLOR());
                        u.setSize(convertStringToEnum(Echelon.class, un.getSIZE()));
                        if (u.getType().equals(UnitType.HEADQUARTERS)) {
                            f.setEchelon(u.getSize());
                        }
                        u.setExperience(convertStringToEnum(Experience.class, un.getEXPERIENCE()));
                        u.setProficiency(un.getPROFICIENCY());
                        u.setReadiness(un.getREADINESS());
                        u.setSupply(un.getSUPPLY());
                        u.setEmphasis(convertStringToEnum(Emphasis.class, un.getEMPHASIS()));
                        u.setReplacementPriority(un.getREPLACEMENTPRIORITY());
                        if (un.getPARENT() != null) {
                            u.setParent(un.getPARENT());
//                            System.out.println(name + " has parent " + un.getPARENT());
                        }
                        List<Equipment> equipmentList = u.getEquipment();
                        for (EQUIPMENT eq : un.getEQUIPMENT()) {
                            Equipment e = new Unit.Equipment();
                            equipmentList.add(e);
                            e.setId(eq.getID());
                            e.setName(eq.getNAME());
                            e.setNumber(eq.getNUMBER());
                            e.setMax(eq.getMAX());
                        }
                    }

                } // End of unit
                if (f.getEchelon() == null) {
                    int echelonLevel = 0;
                    for (Unit u : units) {
                        int echLevel = u.getSize().ordinal();
                        echelonLevel = Math.max(echelonLevel, echLevel);
                    }
                    f.setEchelon(ECHELONS[echelonLevel]);
//                    System.out.println(f.getName() + ": " + f.getEchelon() + " (from subunits) Units: " + units.size());
                } else {
//                    System.out.println(f.getName() + ": " + f.getEchelon() + "(from HQ) Units: " + units.size());
                }

            } // End of formation
        } // End of force
        return o;
    }

    public static Events convert(GAME.EVENTS ev) {
        Events e = new Events();
        // TODO convert events
        return e;
    }

    public static Supplies convert(GAME.SUPPLIES sup) {
        Supplies s = new Supplies();
        // TODO convert supplies
        return s;
    }

    public static Replacements convert(GAME.REPLACEMENTS rep) {
        Replacements r = new Replacements();
        // TODO convert replacements
        return r;
    }

    public static Scenario convert(GAME g) {
        Scenario s = new Scenario();
        scenario = g.getHEADER().getName();
        s.setHeader(convert(g.getHEADER()));
        s.setCalendar(convert(g.getCALENDAR()));
        s.setEnvironment(convert(g.getENVIRONMENT()));
//        s.setForceVariables(convert(g.getFORCEVARIABLES()));
//        s.setVariables(convert(g.getVARIABLES()));
        s.setMap(convert(g.getMAP()));
        s.setOOB(convert(g.getOOB()));
//        s.setEvents(convert(g.getEVENTS()));
//        s.setSupplies(convert(g.getSUPPLIES()));
//        s.setReplacements(convert(g.getREPLACEMENTS()));
        return s;
    }

    static boolean testBitFlag(int mask, int bit) {
        int flag = 1 << bit;
        boolean bitIsSet = (mask & flag) != 0;
        return bitIsSet;
    }

    static Set<Trait> convertToawFlagsToAresTraits(int[] masks) {
        Set<Trait> traits = EnumSet.noneOf(Trait.class);
        for (int i = 0; i < masks.length; i++) {
            int mask = masks[i];
            for (int j = 0; j < 8; j++) {
                if (testBitFlag(mask, j)) {
                    traits.add(TRAITS[i * 8 + j]);
                }
            }
        }
        return traits;
    }

    static <E extends Enum<E>> E convertStringToEnum(Class<E> enumClass, String toawXMLName) {
        String name = toawXMLName.toUpperCase().replace(" ", "_").replace("(", "").replace(")", "");
        return Enum.valueOf(enumClass, name);
    }

    static String shortFormationReport(FORCE.FORMATION f) {
        return scenario + "->" + f.getNAME() + "(" + f.getID() + "): " + f.getORDERS()
                + ", " + f.getEMPHASIS() + ", " + f.getSUPPORTSCOPE();
    }

    static void shortUnitReport(UNIT u, GAME g) {
        System.out.println(g.getHEADER().getName() + "->" + u.getNAME() + "(" + u.getID() + ")" + " Stat "
                + u.getSTATUS());
    }
//    static <E extends Enum<E> & ToawEnumToAres<F>, F extends Enum<F>> java.util.Map<String, F> getMap(Class<E> tetaClass) {
//        java.util.Map<String, F> map = new HashMap<>();
//        for (E teta : tetaClass.getEnumConstants()) {
//            map.put(teta.getToawName(), teta.getAresEnum());
//        }
//        return map;
//    }

    private static boolean isAircraft(Set<Trait> traits) {
        Set<Trait> a = EnumSet.copyOf(traits);
        a.retainAll(AIRCRAFT);
        return !a.isEmpty();
    }

    private static Orders convertOrders(FORMATION fo) {
        ToawOrders toawOrders = convertStringToEnum(ToawOrders.class, fo.getORDERS());
        Orders orders = new Orders();
        orders.setEmphasis(convertStringToEnum(Emphasis.class, fo.getEMPHASIS()));
        orders.setSupportscope(convertStringToEnum(SupportScope.class, fo.getSUPPORTSCOPE()));
        List<Track> tracks = orders.getTrack();
        for (OBJECTIVES objs : fo.getOBJECTIVES()) {
            if (objs.getOBJECTIVE().isEmpty()) {
                break;
            }
            Track track = new Track();
            tracks.add(track);
            track.setId(objs.getTRACK());
            List<Objective> obs = track.getObjective();
            for (OBJECTIVE obj : objs.getOBJECTIVE()) {
                Objective ob = new Objective();
                obs.add(ob);
                ob.setId(obj.getID());
                ob.setX(obj.getX());
                ob.setY(obj.getY());
            }
        }
        orders.setOperationalStance(toawOrders.getStance());
        orders.setFrontage(toawOrders.getFrontage());
        // TODO when TOAW fixes it, use activation turn declared in TOAW
        orders.setActivates(0);
        orders.setOnlyPO(toawOrders.onlyPO());
        return orders;
    }

    private static int transposeIconID(int toawIndex) {
        int rows = 8;
        int columns = 16;
        int row = toawIndex / columns;
        int col = toawIndex % rows;
        int aresIndex = col * rows + row;
        return aresIndex;
    }

}
