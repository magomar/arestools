package arestools.tools;

import ares.application.shared.gui.profiles.GraphicProperties;
import ares.platform.scenario.forces.UnitsColor;
import ares.application.shared.gui.providers.AresMiscTerrainGraphics;
import ares.application.shared.gui.providers.ImageProvider;
import ares.application.shared.gui.providers.ProfiledImageProviderFactory;
import ares.platform.io.FileIO;
import ares.platform.io.ResourcePath;
import ares.platform.scenario.board.Direction;
import ares.platform.scenario.board.Directions;
import ares.platform.scenario.board.Terrain;
import java.awt.Dimension;
import toaw.graphics.ToawGraphicsProfile;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.FileSystems;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import toaw.graphics.ToawMiscTerrainGraphics;
import toaw.data.ToawTerrain;
import toaw.graphics.ToawUnitsColor;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ImportGraphics {

    private static final Logger LOG = Logger.getLogger(ImportGraphics.class.getName());

    public static void convertToawMiscGraphics(ToawGraphicsProfile toawProfile) {
        for (ToawMiscTerrainGraphics toawGraphics : ToawMiscTerrainGraphics.values()) {
            AresMiscTerrainGraphics aresGraphics = toawGraphics.getAresEnum();
            convertToawGraphics(toawGraphics, aresGraphics, toawProfile);
        }
    }

    public static void convertTerrainGraphics(ToawGraphicsProfile toawProfile) {

//        Set<ToawTerrain> toawTerrains = EnumSet.range(ToawTerrain.ARID, ToawTerrain.DEEP_WATER_DECORATOR);
        for (ToawTerrain toawGraphics : ToawTerrain.values()) {
            Terrain aresGraphics = Enum.valueOf(Terrain.class, toawGraphics.getAresEnum().name());
            convertToawGraphics(toawGraphics, aresGraphics, toawProfile);
        }
    }

    public static void convertUnitsGraphics(ToawGraphicsProfile toawProfile) {
        for (ToawUnitsColor toawGraphics : ToawUnitsColor.values()) {
            UnitsColor aresGraphics = Enum.valueOf(UnitsColor.class, toawGraphics.getAresEnum().name());
            convertToawGraphics(toawGraphics, aresGraphics, toawProfile);
        }
    }

    public static void convertBorderGraphics(ToawGraphicsProfile toawProfile) {
        ToawMiscTerrainGraphics toawGraphics = ToawMiscTerrainGraphics.BORDER;
        AresMiscTerrainGraphics aresGraphics = AresMiscTerrainGraphics.TERRAIN_BORDER;
        int profile = toawProfile.getOrdinal();
        ImageProvider toawImageProvider = toawGraphics.createImageProvider(profile);
//        AresGraphicsProfile aresProfile = toawProfile.getAresEnum();
        ImageProvider aresImageProvider = aresGraphics.createImageProvider(profile);
        Dimension dim = aresImageProvider.getFullImageSize();
        BufferedImage aresImage = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = aresImage.createGraphics();
        for (int bitMask = 1; bitMask < 64; bitMask++) {
            Directions directions = Directions.getDirections(bitMask);
            Point aresCoordinates = directions.getCoordinates();
            Set<Direction> dirs = directions.getDirections();
            for (Direction direction : dirs) {
                Point toawCoordinates = new Point(0, direction.ordinal());
                BufferedImage tileImage = toawImageProvider.getImage(toawCoordinates);
                g2.drawImage(tileImage, aresCoordinates.x * tileImage.getWidth(), aresCoordinates.y * tileImage.getHeight(), null);
            }
        }
        g2.dispose();
        File file = FileSystems.getDefault().getPath(
                ResourcePath.GRAPHICS.getFolderPath().toString(),
                GraphicProperties.getProfilePath(profile), aresGraphics.getFilename(profile)).toFile();
        FileIO.saveImage(aresImage, file, "png");
    }

    private static void convertToawGraphics(ProfiledImageProviderFactory toawGraphics, ProfiledImageProviderFactory aresGraphics, ToawGraphicsProfile toawProfile) {
        int profile = toawProfile.getOrdinal();
        ImageProvider toawImageProvider = toawGraphics.createImageProvider(profile);
        BufferedImage toawImage = toawImageProvider.getFullImage();
        ImageProvider aresImageProvider = aresGraphics.createImageProvider(profile);
        Dimension dim = aresImageProvider.getFullImageSize();
        if (toawImage != null) {
            BufferedImage aresImage = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = aresImage.createGraphics();
            int w = Math.min(dim.width, toawImage.getWidth());
            int h = Math.min(dim.height, toawImage.getHeight());
            try {
                g2.drawImage(toawImage.getSubimage(0, 0, w, h), 0, 0, null);
                File file = FileSystems.getDefault().getPath(
                        ResourcePath.GRAPHICS.getFolderPath().toString(),
                        GraphicProperties.getProfilePath(profile), aresGraphics.getFilename(profile)).toFile();
                FileIO.saveImage(aresImage, file, "png");
                LOG.log(Level.INFO, "Image for {0} saved to {1}", new Object[]{((Enum) aresGraphics).name(), file.getAbsolutePath()});
            } catch (Exception e) {
                LOG.log(Level.SEVERE, "Error drawing image");
            }
        }
    }
}
