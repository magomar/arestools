package arestools.gui;

import ares.platform.io.FileIO;
import arestools.io.AresToolsIO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import toaw.data.jaxb.GAME;
import arestools.io.ToawFileType;
import toaw.ToawUtil;
import toaw.data.jaxb.GAME.MAP;

/**
 *
 * @author Mario Gómez Martínez <margomez at dsic.upv.es>
 */
public class ToawAresGUI extends javax.swing.JFrame {

    private static final long serialVersionUID = 1L;
    private File toawFile;
    private File aresFile;

    /**
     * Creates new form ToawAresGUI
     */
    public ToawAresGUI() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
     * content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        inputArea = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        outputArea = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        generateMapMenuItem = new javax.swing.JMenuItem();
        generateScenarioMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentsMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Toaw -> Ares Conversion tool");

        inputArea.setColumns(20);
        inputArea.setEditable(false);
        inputArea.setRows(5);
        jScrollPane2.setViewportView(inputArea);

        jSplitPane1.setLeftComponent(jScrollPane2);

        outputArea.setColumns(20);
        outputArea.setRows(5);
        jScrollPane4.setViewportView(outputArea);

        jSplitPane1.setRightComponent(jScrollPane4);

        jScrollPane1.setViewportView(jSplitPane1);

        jTabbedPane1.addTab("XML Specs", jScrollPane1);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        jTabbedPane1.addTab("OOB", jScrollPane3);

        fileMenu.setMnemonic('f');
        fileMenu.setText("File");

        openMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openMenuItem.setMnemonic('o');
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        saveAsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK));
        saveAsMenuItem.setMnemonic('a');
        saveAsMenuItem.setText("Convert to Ares");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportToAresMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.ALT_MASK));
        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setMnemonic('e');
        editMenu.setText("Edit");

        generateMapMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.ALT_MASK));
        generateMapMenuItem.setText("Generate test map");
        generateMapMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateMapMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(generateMapMenuItem);

        generateScenarioMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK));
        generateScenarioMenuItem.setText("Generate test scenario");
        generateScenarioMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateScenarioMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(generateScenarioMenuItem);

        menuBar.add(editMenu);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Help");

        contentsMenuItem.setMnemonic('c');
        contentsMenuItem.setText("Contents");
        helpMenu.add(contentsMenuItem);

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 557, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void exportToAresMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportToAresMenuItemActionPerformed

        aresFile = AresToolsIO.convertAndMarshallJson(toawFile);
        try {
            FileReader fileReader = new FileReader(aresFile);
            try {
                outputArea.read(fileReader, null);
            } catch (IOException ex) {
                Logger.getLogger(ToawAresGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ToawAresGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_exportToAresMenuItemActionPerformed
    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        JFileChooser fileChooser = new JFileChooser(new File("..\\ToawDataFiles"));
        fileChooser.addChoosableFileFilter(ToawFileType.GAME.getFileTypeFilter());
        fileChooser.addChoosableFileFilter(ToawFileType.EQUIPMENT.getFileTypeFilter());
        int returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                toawFile = fileChooser.getSelectedFile();
                try (FileReader reader = new FileReader(toawFile)) {
                    inputArea.read(reader, "Toaw XML File");
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ToawAresGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ToawAresGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("File access cancelled by user.");
        }
    }//GEN-LAST:event_openMenuItemActionPerformed

    private void generateMapMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateMapMenuItemActionPerformed
        MAP map = ToawUtil.generateTestMap();
        GAME game = new GAME();
        game.setMAP(map);
        File file = new File("data\\tilefeatures.mml");

        try {
            FileIO.marshallJson(game, file);
            FileReader fileReader = new FileReader(file);
            outputArea.read(fileReader, null);
        } catch (IOException ex) {
            Logger.getLogger(ToawUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_generateMapMenuItemActionPerformed

    private void generateScenarioMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateScenarioMenuItemActionPerformed
        GAME game = ToawUtil.generateTestUnitColors(toawFile);
        File file = new File("data\\test.gam");
        try {
            FileIO.marshallJson(game, file);
            FileReader fileReader = new FileReader(file);
            outputArea.read(fileReader, null);
        } catch (IOException ex) {
            Logger.getLogger(ToawUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_generateScenarioMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ToawAresGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ToawAresGUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem contentsMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem generateMapMenuItem;
    private javax.swing.JMenuItem generateScenarioMenuItem;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JTextArea inputArea;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JTextArea outputArea;
    private javax.swing.JMenuItem saveAsMenuItem;
    // End of variables declaration//GEN-END:variables
}
